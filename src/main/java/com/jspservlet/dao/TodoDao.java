package com.jspservlet.dao;

import com.jspservlet.model.Todo;

import java.sql.SQLException;
import java.util.List;

public interface TodoDao {

    void addTodo(Todo todo) throws SQLException;

    Todo selectTodo(long todoId);

    //Method to return all todos of all users to use also uncomment from TodoDaoImpl
    //List<Todo> selectAllTodos();

    boolean deleteTodo(long id) throws SQLException;

    boolean updateTodo(Todo todo) throws SQLException;

    List<Todo> selectTodosByUsername(String uName);

     boolean changeTodoStatus(long id) throws SQLException;

}
