package com.jspservlet.dao;

import com.jspservlet.model.Todo;
import com.jspservlet.model.User;
import com.jspservlet.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {

    private static final String INSERT_USERS_SQL = "INSERT INTO users" +
            " (first_name, last_name, username, password) VALUES " +
            " (?, ?, ?, ?);";
    private static final String FIND_USERS_BY_FNAME_LNAME_SQL = "SELECT * FROM users WHERE username = ?";
    //private static final String UPDATE_USER_SQL = "UPDATE users SET first_name = ?, last_name= ?, username =?, password =?;";

    public int registerUser(User user) throws ClassNotFoundException {

        int result =0;
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
             preparedStatement.setString(1, user.getFirstName());
             preparedStatement.setString(2, user.getLastName());
             preparedStatement.setString(3, user.getUserName());
             preparedStatement.setString(4, user.getPassword());


            System.out.println(preparedStatement);

            result = preparedStatement.executeUpdate(); //execute/update query


        } catch(SQLException e) {
            JDBCUtils.printSQLException(e);
        }
        return result;
    }


    public boolean checkIfUserExists(User user) {
        boolean userExists = false;
        try (Connection connection = JDBCUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_USERS_BY_FNAME_LNAME_SQL)) {
            preparedStatement.setString(1, user.getUserName());
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                userExists = true;
            }
        }
        catch(SQLException e) {
            JDBCUtils.printSQLException(e);
        }
        return userExists;
    }


}
