package com.jspservlet.dao;

import com.jspservlet.model.Todo;
import com.jspservlet.utils.JDBCUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TodoDaoImpl implements TodoDao {
    private static final String INSERT_TODOS_SQL = "INSERT INTO todos" +
            "  (title, username, description, target_date,  is_done) VALUES " + " (?, ?, ?, ?, ?);";

    private static final String SELECT_TODO_BY_ID = "select id,title,username,description,target_date,is_done from todos where id =?";
    //private static final String SELECT_ALL_TODOS = "select * from todos";
    private static final String DELETE_TODO_BY_ID = "delete from todos where id = ?;";
    private static final String UPDATE_TODO = "update todos set title = ?, username= ?, description =?, target_date =?, is_done = ? where id = ?;";
    private static final String SELECT_TODOS_BY_USERNAME = "select * from todos where username = ?;";
    //private static final String GET_USERNAME_TODO = "select username from users;";
    private static final String CHANGE_TODO_STATUS = "UPDATE todos SET is_done='Completed' WHERE id = ?";

    public TodoDaoImpl() {}

    @Override
    public void addTodo(Todo todo) {
        //System.out.println(INSERT_TODOS_SQL);
        try (Connection connection = JDBCUtils.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TODOS_SQL)) {
            preparedStatement.setString(1, todo.getTitle());
            preparedStatement.setString(2, todo.getUsername());
            preparedStatement.setString(3, todo.getDescription());
            preparedStatement.setDate(4, JDBCUtils.getSQLDate(todo.getTargetDate()));
            preparedStatement.setString(5, todo.getStatus());
            System.out.println(preparedStatement);
            int row = preparedStatement.executeUpdate();
            System.out.println(row);
        } catch (SQLException exception) {
            //System.err.format("SQL State: %s\n%s", exception.getSQLState(), exception.getMessage());
            JDBCUtils.printSQLException(exception);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Todo selectTodo(long todoId) {
        Todo todo = null;
        try (Connection connection = JDBCUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TODO_BY_ID)) {
            preparedStatement.setLong(1, todoId);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                long id = rs.getLong("id");
                String title = rs.getString("title");
                String username = rs.getString("username");
                String description = rs.getString("description");
                LocalDate targetDate = rs.getDate("target_date").toLocalDate();
                String isCompleted = rs.getString("is_done");

                todo = new Todo(id, title, username, targetDate, isCompleted, description);
            }
        } catch (SQLException e) {
          JDBCUtils.printSQLException(e);
        }
        return todo;
    }

//    //Method to return all todos of all users to use also uncomment from interface
//    @Override
//    public List<Todo> selectAllTodos() {
//        List<Todo> todos = new ArrayList<>();
//        try (Connection connection = JDBCUtils.getConnection();
//            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TODOS);) {
//            ResultSet rs = preparedStatement.executeQuery();
//
//            while(rs.next()) {
//                long id = rs.getLong("id");
//                String title = rs.getString("title");
//                String username = rs.getString("username");
//                String description = rs.getString("description");
//                LocalDate targetDate = rs.getDate("target_date").toLocalDate();
//                String isCompleted = rs.getString("is_done");
//
//                todos.add(new Todo(id, title, username, targetDate, isCompleted, description));
//            }
//        } catch (SQLException e) {
//            JDBCUtils.printSQLException(e);
//        }
//        return todos;
//    }

    @Override
    public List<Todo> selectTodosByUsername(String uName) {
        List<Todo> todos = new ArrayList<>();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TODOS_BY_USERNAME)) {
            preparedStatement.setString(1, uName);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                long id = rs.getLong("id");
                String title = rs.getString("title");
                String username = rs.getString("username");
                String description = rs.getString("description");
                LocalDate targetDate = rs.getDate("target_date").toLocalDate();
                String isCompleted = rs.getString("is_done");

                todos.add(new Todo(id, title, username, targetDate, isCompleted, description));
            }
        } catch (SQLException e) {
            JDBCUtils.printSQLException(e);
        }
        return todos;
    }

    @Override
    public boolean deleteTodo(long id) throws SQLException {
        boolean todoDeleted;
        try (Connection connection = JDBCUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(DELETE_TODO_BY_ID)) {
            statement.setLong(1, id);
            todoDeleted = statement.executeUpdate() > 0;
        }
        return todoDeleted;
    }

    @Override
    public boolean updateTodo(Todo todo) throws SQLException {
        boolean todoUpdated;
        try (Connection connection = JDBCUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(UPDATE_TODO)) {
            statement.setString(1, todo.getTitle());
            statement.setString(2, todo.getUsername());
            statement.setString(3, todo.getDescription());
            statement.setDate(4, JDBCUtils.getSQLDate(todo.getTargetDate()));
            statement.setString(5, todo.getStatus());
            statement.setLong(6, todo.getId());
            todoUpdated = statement.executeUpdate() > 0;
        }
        return todoUpdated;
    }

    @Override
    public boolean changeTodoStatus(long id) throws  SQLException {
        boolean statusChanged;
        try (Connection connection = JDBCUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(CHANGE_TODO_STATUS)) {
            statement.setLong(1, id);
            statusChanged = statement.executeUpdate() > 0;
        }
        return statusChanged;
    }
}
