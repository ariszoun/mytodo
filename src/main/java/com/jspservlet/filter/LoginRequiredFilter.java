package com.jspservlet.filter;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "*.todo") //which urls does this filter intercept
public class LoginRequiredFilter implements Filter {

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        //cast the servlet request to HttpServletRequest cause all the "handled" requests by this filter are http
        //So, I can direct cast the servletRequest to HttpServletRequest
        HttpServletRequest request = (HttpServletRequest) servletRequest; // that stops the request from proceeding
        if (request.getSession().getAttribute("username") != null) {
            //Proceeding the request with chain.doFilter
            chain.doFilter(servletRequest, servletResponse);
        } else {
            //redirect to login.todo page
            request.getRequestDispatcher("/login.todo").forward(servletRequest, servletResponse);
        }
        //System.out.println(request.getRequestURI());
    }

    @Override
    public void destroy() {

    }
}
