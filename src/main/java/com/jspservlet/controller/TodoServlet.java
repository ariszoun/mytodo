package com.jspservlet.controller;

import com.jspservlet.dao.TodoDao;
import com.jspservlet.dao.TodoDaoImpl;
import com.jspservlet.model.Todo;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/")
public class TodoServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private TodoDao todoDAO;

    public void init() {
        todoDAO = new TodoDaoImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new.todo":
                    showAddTodoForm(request, response);
                    break;
                case "/insert.todo":
                    addTodo(request, response);
                    break;
                case "/delete.todo":
                    deleteTodo(request, response);
                    break;
                case "/edit.todo":
                    showEditTodoForm(request, response);
                    break;
                case "/update.todo":
                    try {
                        updateTodo(request, response);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case "/list.todo":
                    listTodo(request, response);
                    break;
                case "/completed.todo":
                    onClickComplete(request, response);
                    break;
                default:
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/login.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    private void listTodo(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String uName = String.valueOf(request.getSession().getAttribute("username"));
        List<Todo> todos = todoDAO.selectTodosByUsername(uName);
        request.getSession().setAttribute("todos", todos);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/list-todos.jsp");
        dispatcher.forward(request, response);
    }

    private void showAddTodoForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/add-todo.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditTodoForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        long id = Integer.parseInt(request.getParameter("id"));
        Todo existingTodo = todoDAO.selectTodo(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/update-todo.jsp");
        request.setAttribute("todo", existingTodo);
        dispatcher.forward(request, response);
    }

    private void addTodo(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        String title = request.getParameter("title");
        String username = request.getParameter("username");
        String description = request.getParameter("description");
        LocalDate targetDate = LocalDate.parse(request.getParameter("targetDate"));
        String isDone = request.getParameter("isDone");

        Todo newTodo = new Todo();
        newTodo.setTitle(title);
        newTodo.setUsername(username);
        newTodo.setDescription(description);
        newTodo.setTargetDate(targetDate);
        newTodo.setStatus(isDone);

        todoDAO.addTodo(newTodo);
        response.sendRedirect("list.todo");
    }

    private void updateTodo(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        long id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String username = request.getParameter("username");
        String description = request.getParameter("description");
        LocalDate targetDate = LocalDate.parse(request.getParameter("targetDate"));
        String isDone = request.getParameter("isDone");
        Todo updateTodo = new Todo(id, title, username, targetDate, isDone, description);

        todoDAO.updateTodo(updateTodo);
        response.sendRedirect("list.todo");
    }

    private void deleteTodo(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        long id = Integer.parseInt(request.getParameter("id"));
        todoDAO.deleteTodo(id);
        response.sendRedirect("list.todo");
    }

    private void onClickComplete(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        long id = Integer.parseInt(request.getParameter("id"));
        todoDAO.changeTodoStatus(id);
        response.sendRedirect("list.todo");
    }

}
