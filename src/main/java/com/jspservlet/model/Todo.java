package com.jspservlet.model;

import java.time.LocalDate;
import java.util.Objects;

public class Todo {

    private Long id;
    private String title;
    private String username;
    private String description;
    private LocalDate targetDate;
    private String status;

    public Todo() {
    }

    public Todo(Long id, String title, String username, LocalDate targetDate, String status, String description) {
        this.id = id;
        this.title = title;
        this.username = username;
        this.targetDate = targetDate;
        this.status = status;
        this.description = description;
    }

    public Todo(String title, String username, String description, LocalDate targetDate, String status) {
        super();
        this.title = title;
        this.username = username;
        this.description = description;
        this.targetDate = targetDate;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDate getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(LocalDate targetDate) {
        this.targetDate = targetDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String name) {
        this.description = name;
    }

    public String setTodayWarning() {
        if(targetDate.equals(LocalDate.now()))
            return "Expires today...";
        return setDateExpired();
    }

    public String setDateExpired() {
        LocalDate date = LocalDate.now();
        if(date.isAfter(getTargetDate()))
            return "EXPIRED!";
        return targetDate.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(description, todo.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }



}
