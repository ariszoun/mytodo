<%--
  Created by IntelliJ IDEA.
  User: AI21Z
  Date: 7/19/2021
  Time: 4:19 PM
  To change this template use File | Settings | File Templates.
--%>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<%@include file="../common/header.jspf" %>


<nav  class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand" id="navBrand">TODO APP</a>

    <ul class="navbar-nav nav-links">
        <li class="nav-item"><a class="nav-link" href="#">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="https://gitlab.com/ariszoun/mytodo" target="_blank">CheckTheCode</a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto cred-links">
        <li class="active nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/logout.todo">Logout</a></li>
    </ul>
</nav>
<br><br>
<div class="container">
    <div class="jumbotron" style="margin-top: 5%">
        <h1>Welcome <span style="color:#de7c4c"><strong>${username}</strong></span></h1>
        <br>
        <h3 style="margin: 0; padding: 0; font-family: Papyrus,serif; font-weight: bolder">Ready to check your todos?</h3>
        <hr>
        <br>

        <form action="<%=request.getContextPath()%>./list.todo">
            <button class="btn btn-lg btn-primary" type="submit">Check your todos</button>
        </form>
        <form action="<%= request.getContextPath() %>/logout.todo">
            <button class="btn btn-lg btn-warning" type="submit">Go Back &#x21a9;</button>
        </form>
    </div>
</div>

<%@include file="../common/footer.jspf" %>
