<%--
  Created by IntelliJ IDEA.
  User: AI21Z
  Date: 7/19/2021
  Time: 4:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="../common/header.jspf" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<nav  class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand" id="navBrand">TODO APP</a>

    <ul class="navbar-nav nav-links">
        <li class="nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/WelcomeServlet.todo">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/list.todo">Todos</a></li>
        <li class="nav-item"><a class="nav-link" href="https://gitlab.com/ariszoun/mytodo" target="_blank">CheckTheCode</a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto cred-links">
        <li class="active nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/logout.todo">Logout</a></li>
    </ul>
</nav>

<div class="container">
    <br>
    <div class="jumbotron" id="jumbotron">
        <h1>Here you go <span style="color:#de7c4c"><strong>${username}</strong></span></h1>
        <hr>
        <br>
        <h2 style="font-family: Papyrus; font-weight: bolder">How to use this app:</h2>
        <ul id="ul" style="list-style-type: none;">
            <li style="font-family: Papyrus; margin: 10px 0;">Click "Add a new todo" fill out the todo form and click save.</li>
            <li id="more" style="font-family: Papyrus; cursor: pointer; color: blue">more...</li>
            <li id="2" style="font-family: Papyrus; display: none; margin-bottom: 5px">The app will inform you if your todo expires soon or it is already expired.</li>
            <li id="3" style="font-family: Papyrus; display: none; margin-bottom: 5px">To change the status click <span class="btn btn-primary">In-progress</span> to change it from In-Progress to Completed.</li>
            <li id="4" style="font-family: Papyrus; display: none; margin-bottom: 5px">Click <span class="btn btn-warning">&#9998;</span> to change/edit your todo and also change back the status if you want.</li>
            <li id="5" style="font-family: Papyrus; display: none">Click <span class="btn btn-danger">&#10006;</span> to delete your todo.</li>
            <li id="less" style="display:none; font-family: Papyrus; cursor: pointer; color: blue;">...less</li>
        </ul>
        <a id="addNewBtn" class="btn btn-primary"
           href="<%=request.getContextPath()%>/new.todo">Add
            a new todo</a>
        <%--        <form action="/list.todo"><button class="btn btn-lg btn-primary" type="submit">Click to check your todos</button></form>--%>
        <%--        <div>--%>
        <%--            <p id="myToast">Successfully deleted!</p>--%>
        <%--        </div>--%>



    </div>


    <table class="table table-striped">
        <thead class="thead-dark">
        <th>Title</th>
        <th class="textThs">Due Date</th>
        <th class="textThs">Description</th>
        <th>Status</th>
        <th>Action</th>
        </thead>
        <tbody>
        <c:forEach items="${todos}" var="todo">
            <tr>
                <td class="textTds"><c:out value="${todo.title}"/></td>

                <td class="textTds">
                    <c:if test="${todo.setTodayWarning() == 'EXPIRED!'}">
                        <p style="color: red"><strong>${todo.setTodayWarning()}</strong> at: <span style="font-size: smaller">${todo.getTargetDate()}</span></p>
                    </c:if>
                    <c:if test="${todo.setTodayWarning() == 'Expires today...'}">
                        <p style="color: darkorange"><strong>${todo.setTodayWarning()}</strong></p>
                    </c:if>
                    <c:if test="${todo.setTodayWarning() == todo.getTargetDate()}">
                        ${todo.getTargetDate()}
                    </c:if>
                </td>

                <td id="descId">${todo.description}</td>

                <td class="textTds" id="statusBtn">
                    <c:if test="${todo.setTodayWarning() == 'EXPIRED!'}">
                        <a class="btn btn-danger" href="<%=request.getContextPath()%>/completed.todo?id=<c:out value='${todo.id}'/>">
                            <strong>${todo.status}</strong></a>
                    </c:if>
                    <c:if test="${todo.setTodayWarning() == 'Expires today...'}">

                        <a class="btn btn-warning" href="<%=request.getContextPath()%>/completed.todo?id=<c:out value='${todo.id}'/>">
                            <strong>${todo.status}</strong></a>
                    </c:if>
                    <c:if test="${todo.setTodayWarning() == todo.getTargetDate()}">
                        <a class="btn btn-primary" href="<%=request.getContextPath()%>/completed.todo?id=<c:out value='${todo.id}'/>">
                            <strong>${todo.status}</strong></a>
                    </c:if>

                </td>


                <td id="btnTd">
                    <a id="editBtn" class="btn btn-warning"
                       href="edit.todo?id=<c:out value='${todo.id}' />">
                        &#9998;
                    </a>&nbsp;

                        <%--                    <a type="button" id="delBtn" class="btn btn-danger" onclick="return confirm('Are you sure? Todo  [ ${todo.title} ] will be deleted FOREVER AND EVER!')"--%>
                        <%--                       href="delete.todo?id=<c:out value='${todo.id}' />">--%>
                        <%--                        &#10006;--%>
                        <%--                    </a>--%>
                    <a class="btn btn-danger" id="delBtn"
                       data-toggle="modal" href="#myModal" data-todo-id="${todo.id}" data-todo-name="${todo.title}">
                        &#10006;
                    </a>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">This will permanently <span style="color: orangered; font-weight: bolder">DELETE</span> this todo!</h4>
                                </div>
                                <div class="modal-body">
                                    <p>The following todo will be deleted:</p>
                                    <input id="todoId" type="text" name="todoId" value="" style="display: none"/>
                                    <input id="todoName" type="text" name="todoName" value="" readonly style="font-style: italic; border:2px solid orangered; border-radius: 20px; text-align: center"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">No</button>
                                    <a type="button" id="deleteTodoButton" class="btn btn-danger" href="javascript:;"
                                       onclick="
                                         this.href='delete.todo?id=' + document.getElementById('todoId').value;
                                         //deleteAlert();
                                        " >Yes Delete</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>

</div><br><br><br>


<%@include file="../common/footer.jspf" %>

<script>

    // $('#myToast').hide();
    //
    // function hideToast() {
    //     $('#myToast').show();
    //     setTimeout(function() {
    //         $('#myToast').fadeOut('fast');
    //     }, 10000);
    // }

    // function endShowToast() {
    //     $('#myToast').hide();
    // }

    // function deleteAlert() {
    //     alert("Successfully deleted!");
    // }

    $('#myModal').on('show.bs.modal', function(e) {
        var todoId = $(e.relatedTarget).data('todo-id');
        $(e.currentTarget).find('input[name="todoId"]').val(todoId);
    });
    $('#myModal').on('show.bs.modal', function(e) {
        var todoName = $(e.relatedTarget).data('todo-name');
        $(e.currentTarget).find('input[name="todoName"]').val(todoName);
    });

    $("#more").click(function(){
        $("#more").css("display","none");
        $("#2").css("display","block");
        $("#3").css("display","block");
        $("#4").css("display","block");
        $("#5").css("display","block");
        $("#less").css("display","inline");
    });

    $("#less").click(function(){
        $("#less").css("display","none");
        $("#2").css("display","none");
        $("#3").css("display","none");
        $("#4").css("display","none");
        $("#5").css("display","none");
        $("#more").css("display","inline");
    });



    // $(document).ready(function (){
    //     $('.toast').toast('hide');
    // });


    // function showToast() {
    //     // setTimeout(document.getElementById("myToast").style.display="inline", 5000);
    //
    //     var x = document.getElementById("myToast");
    //     if (x.style.visibility === "hidden") {
    //         x.style.visibility = "visible";
    //     }
    //     setTimeout(showToast, 5000);
    // }

    // setTimeout(showToast(), 3000);
    //
    // function hideToast() {
    //     document.getElementById("myToast").style.display="inline";
    //     //setTimeout(toastFunc(), 10000);
    // }
    // setTimeout(hideFunc(), 10000);

</script>

