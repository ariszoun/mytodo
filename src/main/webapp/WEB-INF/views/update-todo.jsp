<%--
  Created by IntelliJ IDEA.
  User: AI21Z
  Date: 7/29/2021
  Time: 2:19 PM
  To change this template use File | Settings | File Templates.
--%>

<%@include file="../common/header.jspf" %>



<nav  class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand" id="navBrand">TODO APP</a>

    <ul class="navbar-nav nav-links">
        <li class="nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/WelcomeServlet.todo">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/list.todo">Todos</a></li>
        <li class="nav-item"><a class="nav-link" href="https://gitlab.com/ariszoun/mytodo" target="_blank">CheckTheCode</a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto cred-links">
        <li class="active nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/logout.todo">Logout</a></li>
        <%--            <li><a href="<%= request.getContextPath() %>/register" class="nav-link">Signup</a></li>--%>
    </ul>
</nav>

<div class="container">

    <br><br>
    <div class="jumbotron" style="margin-top: 5%">
        <h3><strong>Edit todo:</strong></h3>
        <hr>
        <div>
            <c:if test="${todo != null}">
            <form action="update.todo" method="post">
                </c:if>
                <c:if test="${todo == null}">
                <form action="insert.todo" method="post">
                    </c:if>
                    <c:if test="${todo != null}">
                        <input type="hidden" name="id" value="<c:out value='${todo.id}' />"/>
                    </c:if>

                    <fieldset class="form-group">
                        <label>You are logged in as:</label> <input type="text"
                                                                    value="<c:out value='${username}' />" class="form-control"
                                                                    name="username" required="required" readonly>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Todo Title</label> <input type="text"
                                                         value="<c:out value='${todo.title}' />" class="form-control"
                                                         name="title" required="required" minlength="5" maxlength="50">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Todo Description</label>
                        <select class="form-control dropdown" name="description" id="category">
                            <option value="Survival">Survival</option>
                            <option value="Legendary">Legendary</option>
                            <option value="It's OK">It's OK</option>
                            <option value="Could avoid">Could avoid</option>
                            <option value="Avoid">Avoid</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                                                    <select class="form-control"
                                                                                       name="isDone">
                                                    <option value="In Progress">In Progress</option>
                                                    <option value="Completed">Complete</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Due Date</label> <input type="date"
                                                       value="<c:out value='${todo.targetDate}' />"
                                                       class="form-control"
                                                       name="targetDate" required="required">
                    </fieldset>

                    <button type="submit" class="btn btn-success saveBtn">Save</button>
                </form>

                <form action="/list.todo">
                    <button type="submit" class="btn btn-danger cancelBtn">Cancel</button>
                </form>
        </div>
    </div>
</div>



<%@include file="../common/footer.jspf" %>

