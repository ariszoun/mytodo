<%--
  Created by IntelliJ IDEA.
  User: AI21Z
  Date: 7/18/2021
  Time: 4:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/header.jspf" %>


<nav id="nav-bar" class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="" class="navbar-brand" id="navBrandLogin">TODO APP</a>

    <ul class="navbar-nav nav-links">
        <li class="nav-item"><a class="nav-link" href="#">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="https://gitlab.com/ariszoun/mytodo" target="_blank">CheckTheCode</a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto cred-links">
        <li class="active nav-item"><a class="nav-link" href="#">Login</a></li>
        <li><a href="<%= request.getContextPath() %>/register" class="nav-link">Signup</a></li>
    </ul>
</nav>

<div class="container" >


    <br><br>
    <div class="jumbotron" id="jumbotron">
        <h1 style="font-weight: bolder; -webkit-text-stroke: 1px #de7c4c;">JSP TODO Servlet:</h1>
        <h2 style="font-weight: bold; color: #de7c4c; -webkit-text-stroke: 1px black;">Signup or login to enter the app</h2>
        <p style="font-weight: bold; font-family: 'Papyrus'">This is a simple todo app using servlets and JSP for
            training!</p>

        <hr>

        <form action="<%= request.getContextPath() %>/WelcomeServlet.todo" method="post" style="">
            <p class="errorMes">${errorMessage}</p>
            <p class="btn btn-outline-success" style="font-weight: bolder; cursor: default; border: none;">${NOTIFICATION}</p>
            <div class="form-group">
                Enter name:<br> <input type="text" name="username" class="dimens logInput"/><br><br>
            </div>
            <div class="form-group">
                Enter password:<br> <input type="password" name="password" class="dimens logInput"/><br><br>
            </div>

            <button type="submit" class="btn btn-primary dimens">Login</button>

        </form>
    </div>
</div>


<%@include file="../common/footer.jspf" %>
