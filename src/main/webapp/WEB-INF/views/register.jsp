<%--
  Created by IntelliJ IDEA.
  User: AI21Z
  Date: 7/18/2021
  Time: 4:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="../common/header.jspf" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand" id="navBrand">TODO APP</a>

    <ul class="navbar-nav nav-links">
        <li class="nav-item"><a class="nav-link" href="<%= request.getContextPath() %>/login.todo">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="https://gitlab.com/ariszoun/mytodo" target="_blank">CheckTheCode</a>
        </li>
    </ul>

</nav>
<br><br><br><br>

<div class="container">
    <div id="regTitle" class="alert center" role="alert">
        <h2><strong>User Registration</strong></h2>
    </div>

    <div class="jumbotron" id="jumbotron">

        <div>

            <form action="<%= request.getContextPath() %>/register" method="post">
                <p class="errorMes">${registerErrorMessage}</p>
                <div class="form-group">
                    <label for="firstname">First Name:</label> <input type="text" class="form-control" id="firstname" placeholder="First Name" name="firstName" required>
                </div>

                <div class="form-group">
                    <label for="lastname">Last Name:</label> <input type="text" class="form-control" id="lastname" placeholder="last Name" name="lastName" required>
                </div>

                <div class="form-group">
                    <label for="username">User Name:</label> <input type="text" class="form-control" id="username" placeholder="User Name" name="username" required>
                </div>

                <div class="form-group">
                    <label for="password">Password:</label> <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                </div>

                <button type="submit" class="btn btn-primary regSignUpBtn">SignUp</button>

            </form>
            <form class="" action="<%= request.getContextPath()%>/login.todo">
                <button class="btn btn-danger regCancelBtn">Cancel</button>
            </form>
        </div>
    </div>

</div>


<%@include file="../common/footer.jspf" %>
